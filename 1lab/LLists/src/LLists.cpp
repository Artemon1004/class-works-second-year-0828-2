#include "/Projects/templabs/homeedition/1lab/LLists/include/LLists.h"



void LList::searchInfoByName()
{
	string imput;
	int SearchCount(0);
	ITEM* temp = head;
	cout << "Enter the name of the country" << endl;
	getline(cin, imput);
	while (temp)
	{
		if (temp->data->getName() == imput)
		{
			cout << *temp->data << endl;
			SearchCount++;
		}
		
		temp = temp->next;
	}
	if (SearchCount == 0)
		cout << "Can't search info" << endl;
};

void LList::searchInfoByUnCode()
{
	string imput;
	int SearchCount(0);
	ITEM* temp = head;
	cout << "Enter the UdCode of the country " << endl;
	getline(cin, imput);
	while (temp)
	{
		if (temp->data->getUnCode() == imput)
		{
			cout << *temp->data << endl;
			SearchCount++;
		}

		temp = temp->next;
	}
	if (SearchCount == 0)
		cout << "Can't search info" << endl;
};

void LList::searchInfoByCapital()
{
	string imput;
	int SearchCount(0);
	ITEM* temp = head;
	cout << "Enter the capital city of the coutry" << endl;
	getline(cin, imput);
	while (temp)
	{
		if (temp->data->getCapital() == imput)
		{
			cout << *temp->data << endl;
			SearchCount++;
		}

		temp = temp->next;
	}
	if (SearchCount == 0)
		cout << "Can't search info" << endl;
};







LList::ITEM* LList::create(const Country &data)
{
	ITEM *item = new ITEM;
	item->data = new Country;
	*item->data = data;
	item->next = 0;
	return item;
};

LList::LList(const Country &data)
{
	head = create(data);
	tail = head;
}

void LList::AddTail(const Country &data)
{
	if (head&&tail)
	{
		tail->next = create(data);
		tail = tail->next;
	}
	else
	{
		head = create(data);
		tail = head;
	}
}

void LList::AddHead(const Country& data)
{
	if (head&&tail)
	{
		ITEM *temp = create(data);
		temp->next = head;
		head = temp;
	}
	else
	{
		head = create(data);
		tail = head;
	}
};


void LList::Print()const
{
	ITEM *temp = head;
	while (temp)
	{
		cout << *temp->data << endl;
		temp = temp->next;
	}
}
