#include </Projects/templabs/homeedition/1lab/LLists/include/Country.h>

void Country::setLetter1(string str)
{
	letter1 = str;
}

void Country::setLetter2(string str)
{
	letter2 = str;
}

void Country::setName(string str)
{
	name = str;
}

void Country::setCapital(string str)
{
	capital= str;
}

void Country::operator=(const Country& country)
{
	UnCode = country.UnCode;
	letter1 = country.letter1;
	letter2 = country.letter2;
	name = country.name;
	capital = country.capital;
}

void Country::setUnCode(string code)
{
	UnCode = code;
}