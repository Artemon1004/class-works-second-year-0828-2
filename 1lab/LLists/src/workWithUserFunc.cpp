#include</Projects/templabs/homeedition/1lab/LLists/include/LLists.h>
#include<windows.h>

static HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

enum ConsoleColor
{
	Black = 0,
	Blue = 1,
	Green = 2,
	Cyan = 3,
	Red = 4,
	Magenta = 5,
	Brown = 6,
	LightGray = 7,
	DarkGray = 8,
	LightBlue = 9,
	LightGreen = 10,
	LightCyan = 11,
	LightRed = 12,
	LightMagenta = 13,
	Yellow = 14,
	White = 15
};

static void SetColor(ConsoleColor text, ConsoleColor background)//������� ������ ����� ���� � ����
{
	SetConsoleTextAttribute(hStdOut, (WORD)((background << 4) | text));
	system("cls");
}


static void printUserMenu()//����� ���� ��� ������������ 
{
	cout << "Welcome!Let's pick your operation" << endl
		<< "1. Enter all file contents" << endl
		<< "2. Find info using the name of country" << endl
		<< "3. Find info using the capital of country" << endl
		<< "4. Find info using the UnCode of country" << endl
		<< "5. Clear the screen" << endl
		<< "6. Exit" << endl;
}

static void read(LList &temp)//������ �� ����� � ������
{
	cout << "Enter path to csv file" << endl
		<< "example: C:/Users/klufo/Downloads/countries.csv" << endl << endl;
	string str;

	while (1)//����������� �� ��� ��� ���� �� ������ ���������� ���� � �����
	{
		getline(cin, str);
		ifstream fin;
		fin.open(str);
		cout << endl;
		if (fin.is_open())
		{
			fin.close();
			break;
		}
		else
		{
			cout << "Can't find this file. Try it again." << endl;
		}
		
	}
	ifstream fin;
	fin.open(str);
	
	system("cls");
	char buff[256];
	string buffer;
	int Count = 0;

	while (!fin.eof())//����������� ���������� ������ ������ �� ��������� 
	{
		fin.getline(buff, 256);
		buffer = buff;
		string tempfield;
		int last(0);
		Country tempcountry;
		while (buffer.size())
		{

			tempfield = "";
			if (!last)
			{
				int pos = buffer.find_first_of(",");
				tempfield = buffer;
				tempfield.erase(pos, tempfield.size());
				buffer.erase(0, (pos + 1));
			}
			else
			{
				tempfield = buffer;
				buffer = "";
			}

			switch (Count)
			{
			case 0: {tempcountry.setUnCode(tempfield), Count++; break; }
			case 1: {tempcountry.setLetter1(tempfield), Count++; break; }
			case 2: {tempcountry.setLetter2(tempfield), Count++; break; }
			case 3: {tempcountry.setName(tempfield), Count++, last = 1; break; }
			case 4: {tempcountry.setCapital(tempfield), Count++, Count = 0, temp.AddTail(tempcountry);  break; }
			default:
				break;
			}
		}

	}
	fin.close();
}


static int getChoice()
{
	char Choice[256] = "";
	int flag(0);
	while (1)
	{
		
		char tempChoice[256] = "";
		cout << "Enter your choice " << endl;
		cin >> tempChoice;
		cin.ignore();
		for (int i = 0; i < strlen(tempChoice); i++)
		{
			if (!isdigit(tempChoice[i]))
			{
				cout << "Invalid input.Only digits!";
				break;
			}
			else
				if (i ==(strlen(tempChoice)-1)) 
				{
					strcpy(Choice, tempChoice);
					cout << Choice << endl;
					flag++;
					break;
				}
			
		  }
		
		if (flag)
		{
			break;
		}
	}
			

	return atoi(Choice);

}

static void DoItAgain()
{
	string tempChar;
	cout << "Do you wanna continue?(y/n)" << endl;
	cin >> tempChar;
	if ((tempChar == "n")|| (tempChar == "N"))
	{
		exit(1);
	}
	else
	{
		if ((tempChar == "Y") || (tempChar == "y"))
		{
			system("cls");
			printUserMenu();
		}
			
		else
		{
			cout << "Invalid input,try it again" <<endl;
			DoItAgain();
		}

	}
}


static void workWithUser()
{

	LList q;
	read(q);
	string tempForImput = "";
	printUserMenu();
	while (1)
	{
		int temp = getChoice();
		system("cls");
		switch (temp)
		{
		case 1: {q.Print(); DoItAgain();  break; };
		case 2: {q.searchInfoByName();DoItAgain();  break; }
		case 3: {q.searchInfoByCapital(); DoItAgain();  break; }
		case 4: {q.searchInfoByUnCode(); DoItAgain();  break; }
		case 5: {system("cls");printUserMenu();break;}
		case 6: {exit(1);}
		
		default: {cout << "Error imput!" << endl<< endl; DoItAgain(); break; }
		}

	}
}


