#pragma once
#include <stdio.h>
#include <windows.h>
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;


class Country
{
	 string UnCode="",letter1="", letter2 = "", name = "", capital = "";

public:
	Country() : UnCode(""), letter1(""), letter2(""), name(""), capital("") {};
	
	void setUnCode(string code);
	void setLetter1(string str);
	void setLetter2(string str);
	void setName(string str);
	void setCapital(string str);
	string getUnCode() { return UnCode; }
	string getLetter1() { return letter1 ; }
	string getLetter2() { return letter2; }
	string getName() { return name; }
	string getCapital() { return capital; }
	void Country::operator=(const Country& country);

	friend ostream& operator<<(ostream &out, const Country &v)
	{
		out << "UnCode " << v.UnCode   << "   "
			<< "letter1 " << v.letter1 << "   "
			<< "letter2 " << v.letter2 << "   "
			<< "Name of the country "  << setw(41) << left<< v.name << " "
			<< "Capital " << v.capital << "; ";

		return out;

	}
	

	/*friend ostream& operator>>(ostream &in, const Country &v)
	{
		cout << "Enter UnCode" << endl;
		in >> v.UnCode;
		cout << endl
			 << "Enter letter1 "<< endl;
		in >> v.letter1;
		cout << endl
			<< "Enter letter2 " << endl;
		in >> v.letter2; 
		cout << endl
			<< "Enter name " << endl;
		in >> v.name<< endl;
		cout << endl
			<< "Enter capital " << endl;
		in >> v.capital << endl;

		return in;

	}*/ // Later
	
};