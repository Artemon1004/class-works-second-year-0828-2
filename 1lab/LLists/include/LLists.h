#pragma once

#include <stdio.h>
#include <windows.h>
#include <iostream>
#include <fstream>
#include </Projects/templabs/homeedition/1lab/LLists/include/Country.h>
using namespace std;

class LList
{
private:
	struct ITEM {
		Country * data;//��� �����
		ITEM * next;//������ ����� �����
	};
	LList::ITEM*create(const Country&);
	ITEM *head;
	ITEM *tail;
	

public:
	LList() :head(0), tail(0) {}
	LList(const Country&);
	//~LList();
	void AddHead(const Country&);
	void AddTail(const Country&);
	void searchInfoByName();
	void searchInfoByUnCode();
	void searchInfoByCapital();
	Country RmHead();
	Country RmTail();

	void Print() const;
	//can add some

};

