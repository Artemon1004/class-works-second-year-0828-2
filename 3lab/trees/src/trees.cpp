#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;
typedef string Datatype;


struct Node {
	int count;
	Datatype data;
	Node *left;
	Node *right;
};

static int searchMaxCount( Node* root){// ����� ������ ����������� ����� � ������ 
	int tempCount = 0;
	if (root->left) 
	{
		searchMaxCount(root->left);
		if (searchMaxCount(root->left) > tempCount)
			tempCount = searchMaxCount(root->left);
	}
	if (root->count > tempCount)
		tempCount = root->count;
		
	if (root->right) {
		searchMaxCount(root->right);
		if (searchMaxCount(root->right) > tempCount)
			tempCount = searchMaxCount(root->right);
	}
	return tempCount;
}


static void print(Node* root) {//print :D
	if (root->left)
		print(root->left);
	cout << root->data<<endl;
	if (root->right)
		print(root->right);


}


static Node* search(Node* root, const Datatype &data) {//����� � ������
	if (root != 0)
		if (root->data == data)
			return root;
		else
			if (root->data<data)
				return search(root->right, data);
			else
				return search(root->left, data);
	else
		return 0;
}


static Node *addNode(const Datatype &data, Node *root) {//���������� ������ � ������ ������ 
	if (root == NULL) {
		root = new Node;
		root->data = data;
		root->right = NULL;
		root->left = NULL;
		root->count = 1;
	}
	else
		if (root->data>data)
			root->left = addNode(data, root->left);
		else
			if (root->data<data)
				root->right = addNode(data, root->right);
			else
				root->count++;
	return root;
}






